<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('auth')->group(function() {
    Route::post('login', 'Api\AuthController@login');
    Route::get('logout', 'Api\AuthController@logout');
});

Route::prefix('user')->middleware('auth:api')->group(function() {
    Route::get('mutants', 'Api\MutantsController@listMyMutants');
    Route::get('mutants/search', 'Api\MutantsController@search');
});


Route::prefix('mutant')->middleware('auth:api')->group(function() {
    Route::get('', 'Api\MutantsController@index');
    Route::post('', 'Api\MutantsController@store');
    Route::delete('{id}', 'Api\MutantsController@destroy');
});